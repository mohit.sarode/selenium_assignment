package com.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Select_MethodOnSortBy {

	public static void main(String[] args) {
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Books')]")).click();
		 
		WebElement element = driver.findElement(By.id("products-orderby"));  
		Select sel = new Select(element);
		//sel.selectByVisibleText("Price: Low to High");
		//sel.selectByValue("https://demowebshop.tricentis.com/books?orderby=10");
		sel.selectByIndex(3);
		
		
		
	}
}
