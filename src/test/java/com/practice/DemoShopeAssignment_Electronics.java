package com.practice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DemoShopeAssignment_Electronics {

	public static void main(String[] args) throws InterruptedException {

        WebDriver driver= new ChromeDriver();
        driver.get("https://demowebshop.tricentis.com/login");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.id("Email")).sendKeys("mohits3@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("Mohit3");
        driver.findElement(By.xpath("//input[@value='Log in']")).click();
        
        driver.findElement(By.xpath("(//a[contains(text(),'Electronics')])[3]")).click();
        driver.findElement(By.xpath("(//a[contains(text(),'Cell phones')])[3]")).click();
        driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
        driver.findElement(By.xpath("//span[text()='Shopping cart']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@id='termsofservice']")).click();
        driver.findElement(By.id("checkout")).click();
        
//        WebElement country =driver.findElement(By.id("BillingNewAddress_CountryId"));
//        Select sc= new Select(country);
//        sc.selectByIndex(45);
//        driver.findElement(By.id("BillingNewAddress_City")).sendKeys("Mumbai");
//        driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("Karve Road");
//        driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("123456");
//        driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("8888888888");
//        driver.findElement(By.xpath("(//input[@value='Continue'])[1]")).click();
//        Thread.sleep(3000);
        
        driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input[@type='button'])[3]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input[@type='button'])[4]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input[@type='button'])[5]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input[@type='button'])[6]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input[@type='button'])[7]")).click();
        Thread.sleep(2000);
        WebElement output = driver.findElement(By.xpath("//div[@class='section order-completed']"));
        System.out.println(output.getText());    
        Thread.sleep(2000);        
        driver.close();

   
		
	}
}
